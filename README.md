# taxi_app

Démo de Live Activities avec Flutter : Simulation de Trajet
Bienvenue ! Ce projet est une démonstration de l'utilisation des Live Activities dans Flutter, simulant un trajet.

Description

Cette application simule un trajet en utilisant les Live Activities de Flutter. Elle prend en compte le point de départ et le point d'arrivée déjà connus. En utilisant la position actuelle de l'utilisateur, l'application calcule la progression du trajet (progress). Cette valeur de progression, ainsi qu'un booléen (isTermined) (indiquant si le trajet est terminé), sont ensuite transmis à un Widget Extension nommé TrajetWidget, créé dans Xcode.

Pour plus d'informations sur l'intégration des Live Activities avec Flutter, consultez [la documentation officielle](https://pub.dev/packages/live_activities)

Fonctionnalités

Calcul de la progression : L'application calcule dynamiquement la progression du trajet basée sur la position en temps réel de l'utilisateur.
Intégration avec iOS : Grâce à Swift et SwiftUI, une ProgressView et d'autres éléments sont affichés dans le Live Activity sur les appareils iOS.
Configuration et Installation
Assurez-vous d'avoir Flutter installé sur votre système. Pour configurer le projet, suivez ces étapes :

## Getting Started

Clonez le dépôt.
Remplacer "YOUR API KEY" par votre clé d'api dans ios/Runner/AppDelegate.swift (https://pub.dev/packages/google_maps_flutter)

Exécutez flutter pub get pour installer les dépendances.

Ouvrez le projet dans Xcode pour configurer le widget d'extension iOS.

Ajouter un fichier ".gpx" pour simuler le deplacement sur ios (voir : https://digitalbunker.dev/simulating-a-moving-location-in-ios/)

Vous pouvez utiliser le mien pour un test "gpxgenerator_path.gpx" car ce fichier permet de simuler un deplacement du point de depart au point d'arrivé declaré dans le code. 

NB: Si Vous ne simuler pas le deplacement , le live activitie ne sera pas créé (dans le code il est créé uniquement lorsque la position de l'utilisateur change et ensuite il est mis à jour)

Exécutez l'application sur un appareil iOS pour voir les Live Activities en action.




