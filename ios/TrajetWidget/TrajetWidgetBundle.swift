//
//  TrajetWidgetBundle.swift
//  TrajetWidget
//
//  Created by Dogbo Josias Ezechiel on 05/11/2023.
//

import WidgetKit
import SwiftUI

@main
struct TrajetWidgetBundle: WidgetBundle {
    var body: some Widget {
        TrajetWidget()
        TrajetWidgetLiveActivity()
    }
}
