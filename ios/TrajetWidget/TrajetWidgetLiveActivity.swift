//
//  TrajetWidgetLiveActivity.swift
//  TrajetWidget
//
//  Created by Dogbo Josias Ezechiel on 05/11/2023.
//

import ActivityKit
import WidgetKit
import SwiftUI

struct LiveActivitiesAppAttributes: ActivityAttributes, Identifiable {
  public typealias LiveDeliveryData = ContentState // don't forget to add this line, otherwise, live activity will not display it.

  public struct ContentState: Codable, Hashable { }
  
  var id = UUID()
}

let sharedDefault = UserDefaults(suiteName: "group.trajet_widget")!

struct TrajetWidgetLiveActivity: Widget {
    var body: some WidgetConfiguration {
        ActivityConfiguration(for: LiveActivitiesAppAttributes.self) { context in
            
            let progress = sharedDefault.double(forKey: "progress")
            let isTermined = sharedDefault.bool(forKey: "isTermined")
          
            // Lock screen/banner UI goes here
            VStack(alignment: .leading, content: {
                HStack(content: {
                    Image("Taxi")
                        .resizable()
                        .frame(width: 45, height: 45)
                        .cornerRadius(13)
                    
                    VStack(alignment: .leading,content: {
                        let status:String = isTermined ?"Course terminée":"Arrivé dans 1 min"
                        Text(status).font(.system(size: 19, weight: .bold))
                        Text("Toyota corala bleu").font(.system(size: 14))
                    })
                    Spacer()
                    CustomPlaqueVehiculeView()
                   
                })
                
                if (!isTermined){
                    CustomLinearProgressView(progress: progress)
                }else{
                    CustomButtonView(isForExpendedIland: false, title: "Appuyer pour plus de détails")
                    
                }
              
                
               
                             
            }).padding(10)
                .activityBackgroundTint(Color(hex: 0x525052, alpha: 0.5))
            .activitySystemActionForegroundColor(Color.white)

        } dynamicIsland: { context in
            
            DynamicIsland {
                // Expanded UI goes here.  Compose the expanded UI through
                // various regions, like leading/trailing/center/bottom
                DynamicIslandExpandedRegion(.leading) {
                    VStack(content: {
                        Image("Taxi")
                            .resizable()
                            .frame(width: 45, height: 45)
                            .cornerRadius(13)
                        Text("Taxi App")
                    })
                }
                DynamicIslandExpandedRegion(.trailing) {
                    let isTermined = sharedDefault.bool(forKey: "isTermined")
                    let status = isTermined ? "Course Terminée" : "Arrivé dans 1 min"
                    VStack(content: {
                        CustomPlaqueVehiculeView()
                        Text(status).padding([.top],10).font(.system(size: 12))
                    })
                 
                }
        
                DynamicIslandExpandedRegion(.center) {
                    let progress = sharedDefault.double(forKey: "progress")
                    let isTermined = sharedDefault.bool(forKey: "isTermined")
                    
                    if (!isTermined){
                        CustomLinearProgressView(progress: progress)
                    }else{
                        
                        CustomButtonView(isForExpendedIland: true, title: "Appuyer pour plus de détails")
                        
                    }
                }
            } compactLeading: {
                Image("Taxi")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .cornerRadius(13)
            } compactTrailing: {
                let progress = sharedDefault.double(forKey: "progress")
                CircularProgressView(progress: progress).frame(width: 20,height: 20).padding(5)
                        
            } minimal: {
                Text("Taxi")
               /* Image("Taxi")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .cornerRadius(13)*/
            }
            .widgetURL(URL(string: "http://www.apple.com"))
            .keylineTint(Color.red)
        }
    }
}

extension Color {
    init(hex: UInt, alpha: Double = 1) {
        self.init(
            .sRGB,
            red: Double((hex >> 16) & 0xff) / 255,
            green: Double((hex >> 08) & 0xff) / 255,
            blue: Double((hex >> 00) & 0xff) / 255,
            opacity: alpha
        )
    }
}


struct CustomButtonView:View {
    let isForExpendedIland:Bool
    let title:String
    var body: some View{
        ZStack(content: {
            RoundedRectangle(cornerRadius: 10).fill(Color(hex: 0xadadad,alpha: 0.5)).frame(
                height: 50
            )
            Text(title).font(.system(size:isForExpendedIland ? 13 : 16,weight: .bold)).foregroundColor(.white)
        })
    }
}


struct CustomPlaqueVehiculeView:View {
    var body: some View{
        ZStack(content: {
            RoundedRectangle(cornerRadius: 10).fill(Color(hex: 0xadadad,alpha: 0.5)) .frame(width: 100,height: 35)
            Text("23XFKV02").foregroundColor(.white).font(.system(size: 17,weight: .semibold))
            Image("Vehicule")
                .resizable()
              .frame(width: 48, height: 26).offset(y:20)
        })
    }
}


struct CustomLinearProgressView:View {
    let progress:Double
    var body: some View{
        ZStack(alignment: .leading, content: {
            GeometryReader { geometry in
                ProgressView(value: progress ).background(.white).tint(Color(hex:0x2e2e2e, alpha: 0.5)
                    .opacity(0.5)).cornerRadius(20)
                    .padding([.top,.bottom],10)
                Image("Vehicule1")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30, height: 30)
                            .offset(x: CGFloat(progress) * geometry.size.width - 15, y: -3)
            }
          
        }).padding([.top,.bottom],20)
    }
}


struct CircularProgressView: View {
    let progress:Double
    var body: some View{
        ZStack {
            Circle()
                .stroke(
                    .white,
                    lineWidth: 2
                )
            Circle()
                .trim(from: 0, to: progress)
                .stroke(
                    Color(hex: 0xadadad),
                    style: StrokeStyle(
                        lineWidth:2,
                        lineCap: .round
                    )

                )
                // 1
                .rotationEffect(.degrees(-90))
            
            let angle = 360 * progress - 90
                      let x = cos(Angle(degrees: angle).radians) * 10
                      let y = sin(Angle(degrees: angle).radians) * 10
            
            Circle()
                            .frame(width: 5, height: 5)
                            .foregroundColor(.white)
                            .offset(x: CGFloat(x), y: CGFloat(y))

        }
    }
}
