import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:live_activities/live_activities.dart';
import 'package:lottie/lottie.dart';

class EndPage extends StatefulWidget {
  const EndPage({super.key});

  @override
  State<EndPage> createState() => _EndPageState();
}

class _EndPageState extends State<EndPage> {
  final liveActivitiesPlugin = LiveActivities();

  @override
  void initState() {
    liveActivitiesPlugin.endAllActivities();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 200,
                height: 200,
                child: LottieBuilder.asset(
                  "assets/succes.json",
                  repeat: false,
                ),
              ),
              Text(
                'Course terminée',
                style: GoogleFonts.outfit(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 15),
              Text(
                'Votre course a été terminée avec succès',
                style: GoogleFonts.outfit(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Text(
                "Merci d'avoir choisi Taxi App ! Nous apprécions votre confiance en notre service. Nous espérons que votre trajet a été agréable et que vous êtes arrivé(e) à destination en toute sécurité.😍!",
                textAlign: TextAlign.center,
                style: GoogleFonts.outfit(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
