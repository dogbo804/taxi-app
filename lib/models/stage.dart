class Stage {
  final String sid;
  final String snm;
  final String scd;
  final String badgeUrl;
  final String firstColor;
  final String cnm;
  final String csnm;
  final String ccd;
  final String compId;
  final String compN;
  final String compD;
  final String compSt;
  final int scu;
  final List<Event> events;

  Stage({
    required this.sid,
    required this.snm,
    required this.scd,
    required this.badgeUrl,
    required this.firstColor,
    required this.cnm,
    required this.csnm,
    required this.ccd,
    required this.compId,
    required this.compN,
    required this.compD,
    required this.compSt,
    required this.scu,
    required this.events,
  });

  Stage copyWith({
    String? sid,
    String? snm,
    String? scd,
    String? badgeUrl,
    String? firstColor,
    String? cnm,
    String? csnm,
    String? ccd,
    String? compId,
    String? compN,
    String? compD,
    String? compSt,
    int? scu,
    List<Event>? events,
  }) =>
      Stage(
        sid: sid ?? this.sid,
        snm: snm ?? this.snm,
        scd: scd ?? this.scd,
        badgeUrl: badgeUrl ?? this.badgeUrl,
        firstColor: firstColor ?? this.firstColor,
        cnm: cnm ?? this.cnm,
        csnm: csnm ?? this.csnm,
        ccd: ccd ?? this.ccd,
        compId: compId ?? this.compId,
        compN: compN ?? this.compN,
        compD: compD ?? this.compD,
        compSt: compSt ?? this.compSt,
        scu: scu ?? this.scu,
        events: events ?? this.events,
      );
}

class Event {
  final String eid;
  final Map<String, String> pids;
  final Map<String, List<Media>> media;
  final String tr1;
  final String tr2;
  final String trh1;
  final String trh2;
  final String tr1Or;
  final String tr2Or;
  final List<T1> t1;
  final List<T1> t2;
  final String eps;
  final int esid;
  final int epr;
  final int ecov;
  final String ernInf;
  final int ewt;
  final int et;
  final int esd;
  final int eo;
  final int eox;
  final int spid;
  final int pid;
  final int? ls6;

  Event({
    required this.eid,
    required this.pids,
    required this.media,
    required this.tr1,
    required this.tr2,
    required this.trh1,
    required this.trh2,
    required this.tr1Or,
    required this.tr2Or,
    required this.t1,
    required this.t2,
    required this.eps,
    required this.esid,
    required this.epr,
    required this.ecov,
    required this.ernInf,
    required this.ewt,
    required this.et,
    required this.esd,
    required this.eo,
    required this.eox,
    required this.spid,
    required this.pid,
    this.ls6,
  });

  Event copyWith({
    String? eid,
    Map<String, String>? pids,
    Map<String, List<Media>>? media,
    String? tr1,
    String? tr2,
    String? trh1,
    String? trh2,
    String? tr1Or,
    String? tr2Or,
    List<T1>? t1,
    List<T1>? t2,
    String? eps,
    int? esid,
    int? epr,
    int? ecov,
    String? ernInf,
    int? ewt,
    int? et,
    int? esd,
    int? eo,
    int? eox,
    int? spid,
    int? pid,
    int? ls6,
  }) =>
      Event(
        eid: eid ?? this.eid,
        pids: pids ?? this.pids,
        media: media ?? this.media,
        tr1: tr1 ?? this.tr1,
        tr2: tr2 ?? this.tr2,
        trh1: trh1 ?? this.trh1,
        trh2: trh2 ?? this.trh2,
        tr1Or: tr1Or ?? this.tr1Or,
        tr2Or: tr2Or ?? this.tr2Or,
        t1: t1 ?? this.t1,
        t2: t2 ?? this.t2,
        eps: eps ?? this.eps,
        esid: esid ?? this.esid,
        epr: epr ?? this.epr,
        ecov: ecov ?? this.ecov,
        ernInf: ernInf ?? this.ernInf,
        ewt: ewt ?? this.ewt,
        et: et ?? this.et,
        esd: esd ?? this.esd,
        eo: eo ?? this.eo,
        eox: eox ?? this.eox,
        spid: spid ?? this.spid,
        pid: pid ?? this.pid,
        ls6: ls6 ?? this.ls6,
      );
}

class Media {
  final Type? type;
  final List<AllowedCountry>? allowedCountries;
  final String? eventId;
  final Provider? provider;
  final String? streamhls;
  final List<dynamic>? deniedCountries;
  final String? ageRestricted;
  final String? articleId;

  Media({
    this.type,
    this.allowedCountries,
    this.eventId,
    this.provider,
    this.streamhls,
    this.deniedCountries,
    this.ageRestricted,
    this.articleId,
  });

  Media copyWith({
    Type? type,
    List<AllowedCountry>? allowedCountries,
    String? eventId,
    Provider? provider,
    String? streamhls,
    List<dynamic>? deniedCountries,
    String? ageRestricted,
    String? articleId,
  }) =>
      Media(
        type: type ?? this.type,
        allowedCountries: allowedCountries ?? this.allowedCountries,
        eventId: eventId ?? this.eventId,
        provider: provider ?? this.provider,
        streamhls: streamhls ?? this.streamhls,
        deniedCountries: deniedCountries ?? this.deniedCountries,
        ageRestricted: ageRestricted ?? this.ageRestricted,
        articleId: articleId ?? this.articleId,
      );
}

enum AllowedCountry { GB, IE, NL }

enum Provider { ABELSON }

enum Type { RESULT, TV_CHANNEL, TWITTER_HIGHLIGHTS }

class T1 {
  final String nm;
  final String id;
  final String img;
  final String newsTag;
  final String abr;

  T1({
    required this.nm,
    required this.id,
    required this.img,
    required this.newsTag,
    required this.abr,
  });

  T1 copyWith({
    String? nm,
    String? id,
    String? img,
    String? newsTag,
    String? abr,
  }) =>
      T1(
        nm: nm ?? this.nm,
        id: id ?? this.id,
        img: img ?? this.img,
        newsTag: newsTag ?? this.newsTag,
        abr: abr ?? this.abr,
      );
}
