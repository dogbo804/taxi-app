// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:foot_app/end_page.dart';
import 'package:foot_app/maps.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:live_activities/live_activities.dart';

import 'package:zoom_tap_animation/zoom_tap_animation.dart';

const double maxDistance = 5;
const destination = LatLng(5.355520890626714, -4.100912418598936);
const depart = LatLng(5.357302203948251, -4.100927899999998);

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Taxi app',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.teal),
        useMaterial3: true,
        textTheme: GoogleFonts.outfitTextTheme(),
      ),
      home: const MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool septTwo = false;
  final liveActivitiesPlugin = LiveActivities();
  String? activityID;
  late Timer timer;
  double progress = 0;
  StreamSubscription<Position>? streamPosition;

  @override
  void initState() {
    if (Platform.isAndroid) {
      return;
    }

    liveActivitiesPlugin.init(appGroupId: "group.trajet_widget");

    streamPosition = Geolocator.getPositionStream(
            locationSettings: const LocationSettings(distanceFilter: 4))
        .listen((event) async {
      progress = calculateProgress(
          depart.latitude,
          depart.longitude,
          //
          destination.latitude,
          destination.longitude,
          //
          event.latitude,
          event.longitude);
      print('Progress: $progress');
      final Map<String, dynamic> activityModel = {
        "progress": progress >= 0.97 ? 1 : progress,
        "isTermined": progress >= 0.97,
      };

      if (activityID == null) {
        activityID = "INIT_ACTIVITY_ID";
        activityID = await liveActivitiesPlugin.createActivity(
          activityModel,
          removeWhenAppIsKilled: true,
        );
        print("----------------$activityID--------------------");
      } else {
        liveActivitiesPlugin.updateActivity(activityID!, activityModel);
      }
      double distance = Geolocator.distanceBetween(
        event.latitude,
        event.longitude,
        destination.latitude,
        destination.longitude,
      );
      if (distance <= maxDistance) {
        print("Vous êtes proche de la destination !");

        if (septTwo) {
          streamPosition?.cancel();
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) {
              return const EndPage();
            }),
          );
        } else {
          septTwo = true;
        }
      } else {
        print("Vous n'êtes pas encore arrivé.");
      }
    });

    // liveActivitiesPlugin.activityUpdateStream.listen((event) {
    //   event.map(
    //     active: (activity) {
    //       // Get the token
    //       print(activity.activityToken);
    //     },
    //     ended: (activity) {},
    //     unknown: (activity) {},
    //     stale: (StaleActivityUpdate value) {},
    //   );
    // });

    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    liveActivitiesPlugin.dispose();
    streamPosition?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final e = MediaQuery.of(context).size;
    final heigth = e.height;

    return Scaffold(
      body: Platform.isAndroid
          ? const Center(
              child: Text("Configuration android non effectuée"),
            )
          : Column(
              children: [
                Expanded(
                  child: SizedBox(
                    height: heigth / 1.5,
                    child: const MapWidget(
                      latUser: 5.357602131444049,
                      lngUser: -4.100818814798721,
                      polylines: {},
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(
                      right: 20, left: 20, bottom: 20, top: 10),
                  child: CustomButton(
                    onTap: () {},
                  ),
                ),
              ],
            ),
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({super.key, required this.onTap});

  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            colors: [
              Color(0xff8C52FF),
              Color(0xff1C47A8),
            ],
          ),
        ),
        child: Center(
          child: Text('Une course est lancée...',
              style: GoogleFonts.outfit(
                fontSize: 16,
                color: Colors.white,
                fontWeight: FontWeight.w700,
              )),
        ),
      ),
    );
  }
}

double calculateProgress(double latA, double lonA, double latB, double lonB,
    double latCurrent, double lonCurrent) {
  double totalDistance = sqrt(pow(latB - latA, 2) + pow(lonB - lonA, 2));
  double remainingDistance =
      sqrt(pow(latB - latCurrent, 2) + pow(lonB - lonCurrent, 2));
  double progress =
      (totalDistance - remainingDistance).clamp(0.0, totalDistance) /
          totalDistance;
  return progress;
}
