import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foot_app/main.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapWidget extends StatefulWidget {
  final void Function(GoogleMapController)? onMapCreated;

  final Set<Polyline> polylines;
  final double? latUser;
  final double? lngUser;

  final CameraPosition? initialCameraPosition;
  final bool zoomGesturesEnabled;
  final void Function(LatLng)? onTap;
  const MapWidget(
      {super.key,
      this.zoomGesturesEnabled = false,
      required this.polylines,
      this.initialCameraPosition,
      this.onTap,
      this.latUser,
      this.onMapCreated,
      this.lngUser});

  @override
  State<MapWidget> createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget>
    with AutomaticKeepAliveClientMixin {
  CameraPosition? initialCameraPosition;
  Completer<GoogleMapController> mapController =
      Completer<GoogleMapController>();

  Set<Marker> markers = {};

  BitmapDescriptor? customIcon;
  String? mapStyle;

  Future<void> getUserLocation() async {
    if (widget.latUser != null) {
      initialCameraPosition = CameraPosition(
        target: LatLng(widget.latUser!, widget.lngUser!),
        zoom: 16.5,
      );
      setState(() {});
    }
  }

  @override
  void initState() {
    // make sure to initialize before map loading
    rootBundle.loadString('assets/map.txt').then((v) {
      mapStyle = v;
      setState(() {});
    });

    BitmapDescriptor.fromAssetImage(
            const ImageConfiguration(size: Size(12, 12)), 'assets/flag.png')
        .then((d) async {
      customIcon = d;
      await addMarker();
    });

    getUserLocation();

    super.initState();
  }

  Future<void> addMarker() async {
    // for add flag marker
    const MarkerId markerId = MarkerId('destination');
    final Uint8List markerIcon = await getBytesFromAsset('assets/flag.png', 80);

    // creating a new MARKER
    final Marker marker = Marker(
      icon: BitmapDescriptor.fromBytes(markerIcon),
      markerId: markerId,
      position: destination,
      infoWindow:
          const InfoWindow(title: "Arrivé", snippet: "votre destination"),
    );

    setState(() {
      markers.add(marker);
    });
  }

  @override
  dispose() {
    super.dispose();
    if (mounted) {
      _disposeController();
    }
  }

  Future<void> _disposeController() async {
    final GoogleMapController controller = await mapController.future;

    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return (initialCameraPosition == null &&
            widget.initialCameraPosition == null)
        ? const Center(child: CircularProgressIndicator())
        : GoogleMap(
            myLocationEnabled: true,
            zoomGesturesEnabled: true,
            initialCameraPosition: CameraPosition(
                target: LatLng(widget.latUser!, widget.lngUser!),
                zoom: 17,
                bearing: 20),
            onMapCreated: (GoogleMapController controller) async {
              mapController.complete(controller);
              controller.setMapStyle(mapStyle);
            },
            markers: markers,
            polylines: widget.polylines,
            onTap: widget.onTap,
          );
  }

  @override
  bool get wantKeepAlive => false;
}

Future<Uint8List> getBytesFromAsset(String path, int width) async {
  ByteData data = await rootBundle.load(path);
  ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
      targetWidth: width);
  ui.FrameInfo fi = await codec.getNextFrame();
  return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
      .buffer
      .asUint8List();
}
